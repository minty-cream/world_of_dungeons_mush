TODO
----

Create rules.py file
Figure out why character sheet becomes blank after restart
Write news files (none currently written, if this is ever untrue make a list of everything that should have a news file. Work from there.)
Implement system that allows GM to request a roll for an action with reason and examples of possible consequences, a player to either agree or suggest a different stat with reason, and a GM to approve or disapprove with reason.

Backlog
-------

Create first dungeon
Create opening area
Create shop
Create storage

Wish-list
---------

Implement word ranking system. WRS takes paragraphs with a description that is one of the six stats. All words have six stats. Every time they are found in a paragraph, they gain a point in that stat.
Patch skills to the Roll Request System
Patch skills to the Word Ranking System
Create Automated Roll Suggestion feature
Automated Roll Suggestion feature uses six trained simple neural networks, one for each stat, and suggests that a paragraph is the stat who's equivalent NN returns the highest value.
