"""
Characters

Characters are (by default) Objects setup to be puppeted by Players.
They are what you "see" in game. The Character class in this module
is setup to be the "default" character type created by the default
creation commands.

"""
from evennia import DefaultCharacter
from evennia.utils import evform
from evennia.utils import logger

class Character(DefaultCharacter):
    """
    The Character defaults to reimplementing some of base Object's hook methods with the
    following functionality:

    at_basetype_setup - always assigns the DefaultCmdSet to this object type
                    (important!)sets locks so character cannot be picked up
                    and its commands only be called by itself, not anyone else.
                    (to change things, use at_object_creation() instead).
    at_after_move - Launches the "look" command after every move.
    at_post_unpuppet(player) -  when Player disconnects from the Character, we
                    store the current location in the pre_logout_location Attribute and
                    move it to a None-location so the "unpuppeted" character
                    object does not need to stay on grid. Echoes "Player has disconnected" 
                    to the room.
    at_pre_puppet - Just before Player re-connects, retrieves the character's
                    pre_logout_location Attribute and move it back on the grid.
    at_post_puppet - Echoes "PlayerName has entered the game" to the room.

    """

    def at_object_creation(self):
        "called only once, when object is first created"
        # we will use this to stop player from changing sheet
        self.db.sheet_locked = False 
        # we store these so we can build these on demand
        self.db.chardata  = {
        "STR": 3,
        "CON": 2,
        "DEX": 2,
        "INT": 1,
        "WIS": 1,
        "CHA": 0,
        "SKILL1": "",
        "SKILL2": "",
        "SKILL3": "",
        "SKILL4": "",
        "SKILL5": "",
        "SP1": "",
        "SP2": "",
        "SP3": "",
        "SP4": "",
        "SP5": "",
        "ARMOR": 0,
        "SPEED": "FAST",
        "SHIELD": 0,
        "TOTAL_ARMOR": 0,
        "HD": 1,
        "HP": 6,
        "NOTES": "",
        "COIN": 60,
        "NEXTXP": 1000,
        "XP": 0,
        "LVL": 1,
        "CLASS": ""
        }
        self.db.charsheet = evform.EvForm("world/charsheetform.py")
        logger.log_info("Sheet initialized:")
        logger.log_info(unicode(self.db.charsheet))
        self.update_charsheet()

    def update_charsheet(self):
        """
        Call this to update the sheet after any of the ingoing data
        has changed.
        """
        data = dict(self.db.chardata)
        #This makes the character sheet happy
        data['NAME'] = str(self.key)
        data['TA'] = data['TOTAL_ARMOR']
        data['1'] = data['STR']
        data['2'] = data['DEX']
        data['3'] = data['CON']
        data['4'] = data['INT']
        data['5'] = data['WIS']
        data['6'] = data['CHA']
        for stat in ['TOTAL_ARMOR','STR','DEX','CON','INT','WIS','CHA']:
            del data[stat]
        self.db.charsheet.map(cells=data)
