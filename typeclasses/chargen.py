"""
Room

Rooms are simple containers that has no location of their own.

"""

from evennia import DefaultRoom
from evennia import Command
from evennia import default_cmds


class ChargenRoom(DefaultRoom):
    """
    Rooms are like any Object, except their location is None
    (which is default). They also use basetype_setup() to
    add locks so they cannot be puppeted or picked up.
    (to change that, use at_object_creation instead)

    See examples/object.py for a list of
    properties and methods available on all Objects.
    """
    def at_object_creation(self):
        self.cmdset.add_default(CharGenCmdSet)

class CmdCharGen(Command):
    key = "chargen"
    aliases = ("start","cg")

    def func(self):
        self.caller.db.sheet_locked = False
        self.msg("Character sheet unlocked. Begin editing with @sheet.")

class CmdFinishCharGen(Command):
    key = "finish_chargen"
    aliases = ("finish","fcg")

    def func(self):
        #Planning for the future needlessly. Levelup-ready
        #Check to make sure the stat total is correct
        stat_total=0
        stat_total_count=9+((self.caller.db.chardata['LVL']-1)/3)

        skill_count=2+((self.caller.db.chardata['LVL']/3))
        skill_list=["athletics","awareness","deception","decipher","heal","leadership","lore","stealth","survival"]

        ability_count=2+((self.caller.db.chardata['LVL']/3))
        ability_list=["skirmish","tough","slay","hardy","backstab","lucky","reflexes","tinker","bless","cure","turn","vision","summon","cantrips","command","ritual"]

        for stat in ['STR','CON','DEX','INT','WIS','CHA']:
            if not (int(self.caller.db.chardata[stat]) < 4):
                self.msg(stat+" incorrect, no stat should be above 3. "+stat+" is "+self.caller.db.chardata[stat])
                return
            stat_total+=int(self.caller.db.chardata[stat])
        if(stat_total > stat_total_count or stat_total < stat_total_count):
            self.msg("Stats incorrect, should total "+str(stat_total_count)+".")
            return
        for skill in range(1,5):
            skill_str = "SKILL"+str(skill)
            if(skill <= skill_count):
                real_skill = False
                for skill_name in skill_list:
                    if (skill_name == self.caller.db.chardata[skill_str]):
                        real_skill = True
                if not real_skill:
                    self.msg("Skills incorrect, skills should be one of the following: "+", ".join(skill_list))
                    return
            else:
                    self.caller.db.chardata[skill_str]=""
        for ability in range(1,5):
            ability_str = "SP"+str(ability)
            if(ability <= ability_count):
                real_ability = False
                for ability_name in ability_list:
                    if (ability_name == self.caller.db.chardata[ability_str]):
                        real_ability = True
                if not real_ability:
                    self.msg("Abilities incorrect, abilitys should be one of the following: "+", ".join(ability_list))
                    return
            else:
                if not (self.caller.db.chardata[ability_str] == ""):
                    self.caller.db.chardata[ability_str]=""
        self.caller.db.sheet_locked = True
        self.msg("Character sheet locked.")

class CharGenCmdSet(default_cmds.CharacterCmdSet):
    def at_cmdset_creation(self):
        self.add(CmdCharGen())
        self.add(CmdFinishCharGen())

