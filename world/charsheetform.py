#coding=utf-8

# in mygame/world/charsheetform.py

FORMCHAR = "x"
TABLECHAR= "c"

FORM = """
.-----------------------------------------------------------------------------.
|                                                                             |
| Name: xxxxxxxxxxxxxxxxxNAMExxxxxxxxxxxxxxxxxxx Class xxCLASSxxxx Lv xLVLxx  |
|                                                                             |
 >----------------------------------------------------------------------------<
| ATTRIBUTES        SKILLS: xSKILL1xx xSKILL2xx xSKILL3xx xSKILL4xx xSKILL5xx |
| ----------        SPECIAL ABILITIES                                         |
| x1x STR x4x INT   -----------------                                         |
| x2x DEX x5x WIS   xxSP1xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx |
| x3x CON x6x CHA   xxSP2xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx |
|                   xxSP3xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx |
|                   xxSP4xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx |
|                   xxSP5xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx |
 >----------------------------------------------------------------------------<
| ARMOR[ xxxxARMORxxxx ] SHIELD[ xSHIELDx ] TOTAL ARMOR[ xTAx ]               |
| HD[ xHDx ] HP[ xHPx ] SPEED[ xxSPEEDxx ]                                    |
 >----------------------------------------------------------------------------<
| NOTES                                                                       |
| xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx |
| xxxxxxxxxxxxxxxxxxxxxxxxxxNOTESxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx |
| xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx |
| xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx |
 >----------------------------------------------------------------------------<
| COIN[ xCOINx ] NEEDED FOR NEXT LEVEL[ xxxxNEXTXPxxxx ] XP[ xxxxxxXPxxxxxx ] |
 >----------------------------------------------------------------------------<
"""
