def roll_stat(stat):
    #Rolls the characters stat, and returns either "Miss", "Partial Success", "Full Success" or "Critical Success".
    roll = randint(1,6) + randint(1,6) #PbTA likes its 2d6
    roll = roll + stat
    if(roll >= 12):
        return "Critical Success"
    elif(roll>=10):
        return "Full Success"
    elif(roll>=7):
        return "Partial Success"
    else:
        return "Miss"

def roll_damage(character, target, ranged=False):
    #might be better as a command? Theres not really much rules involved here
    if(ranged):
        #apply ranged bonuses
        return -1
    else:
        #apply melee bonuses
        return -1
    #deal damage with equipped weapon to target

def roll_health(hd, lvl, hasHealer=False):
    health_rolls = [6] #First roll is always six
    if(hasHealer):
        health_rolls.push(6) #Healers always ensure second roll is six
    for x in range(health_rolls.length,hd):
        health_rolls.push(randint(1,6))
    health_rolls.sort()
    health_rolls.flip()
    health = 0
    for x in range(1,lvl):
        health = health + health_rolls.pop()
    return health
